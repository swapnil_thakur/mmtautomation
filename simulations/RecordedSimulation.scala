
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class RecordedSimulation extends Simulation {

	val httpProtocol = http
		.baseURL("http://www.xpanxion.com")
		.inferHtmlResources()

	val headers_0 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, sdch",
		"Accept-Language" -> "en-US,en;q=0.8",
		"Connection" -> "keep-alive",
		"Upgrade-Insecure-Requests" -> "1",
		"User-Agent" -> "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36")

	val headers_1 = Map("User-Agent" -> "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36")

	val headers_3 = Map(
		"Accept" -> "image/webp,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, sdch",
		"Accept-Language" -> "en-US,en;q=0.8",
		"Connection" -> "keep-alive",
		"User-Agent" -> "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
		"X-Client-Data" -> "CLO1yQEIl7bJAQiktskBCMG2yQEI+5zKAQipncoB")

	val headers_18 = Map(
		"Accept" -> "image/webp,image/*,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate, sdch",
		"Accept-Language" -> "en-US,en;q=0.8",
		"Connection" -> "keep-alive",
		"User-Agent" -> "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36")

	val headers_22 = Map(
		"Accept" -> "*/*",
		"Accept-Encoding" -> "gzip, deflate, sdch",
		"Accept-Language" -> "en-US,en;q=0.8",
		"Connection" -> "keep-alive",
		"User-Agent" -> "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
		"X-Client-Data" -> "CLO1yQEIl7bJAQiktskBCMG2yQEI+5zKAQipncoB")

    val uri1 = "http://maps.googleapis.com"
    val uri2 = "http://csi.gstatic.com/csi"
    val uri3 = "http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/css/bootstrap.css"
    val uri4 = "http://fonts.gstatic.com/s"
    val uri6 = "http://fonts.googleapis.com/css"

	val scn = scenario("RecordedSimulation")
		.exec(http("request_0")
			.get("/contact/")
			.headers(headers_0)
			.resources(http("request_1")
			.get("/wp-includes/js/twemoji.js?ver=4.3.6")
			.headers(headers_1),
            http("request_2")
			.get("/wp-includes/js/wp-emoji.js?ver=4.3.6")
			.headers(headers_1),
            http("request_3")
			.get(uri2 + "?v=2&s=mapsapi3&v3v=27.8&action=apiboot2&e=10_1_0,10_2_0&rt=main.5")
			.headers(headers_3),
            http("request_4")
			.get("/wp-content/themes/accesspress-parallax/css/styles/services.css")
			.headers(headers_1),
            http("request_5")
			.get("/wp-content/themes/accesspress-parallax/css/styles/approach.css")
			.headers(headers_1),
            http("request_6")
			.get("/wp-content/themes/accesspress-parallax/css/styles/home.css")
			.headers(headers_1),
            http("request_7")
			.get("/wp-content/themes/accesspress-parallax/css/styles/footer.css")
			.headers(headers_1),
            http("request_8")
			.get(uri6 + "?family=Source+Sans+Pro:400|Open+Sans:300"),
            http("request_9")
			.get(uri4 + "/oxygen/v5/78wGxsHfFBzG7bRkpfRnCQ.woff2"),
            http("request_10")
			.get(uri4 + "/roboto/v15/Hgo13k-tfSpn0qi1SFdUfVtXRa8TVwTICgirnJhmVJw.woff2"),
            http("request_11")
			.get(uri4 + "/roboto/v15/CWB0XYA8bzo0kSThX0UTuA.woff2"),
            http("request_12")
			.get("/wp-content/plugins/fruitful-shortcodes/includes/shortcodes/fonts/fontawesome-webfont.woff?v=4.2.0"),
            http("request_13")
			.get(uri4 + "/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTegdm0LZdjqr5-oayXSOefg.woff2"),
            http("request_14")
			.get(uri4 + "/sourcesanspro/v9/ODelI1aHBYDBqgeIAH2zlNV_2ngZ8dMf8fLgjYEouxg.woff2"),
            http("request_15")
			.get("/wp-content/themes/accesspress-parallax/images/logo.png"),
            http("request_16")
			.get("/wp-content/themes/accesspress-parallax/css/images/sprite-image.png"),
            http("request_17")
			.get("/wp-content/plugins/contact-form-7/images/ajax-loader.gif")
			.headers(headers_1)))
		.pause(1)
		.exec(http("request_18")
			.get("/contact/")
			.headers(headers_18))
		.pause(3)
		.exec(http("request_19")
			.get(uri1 + "/maps-api-v3/api/js/27/8/common.js")
			.headers(headers_1)
			.resources(http("request_20")
			.get(uri1 + "/maps-api-v3/api/js/27/8/util.js")
			.headers(headers_1),
            http("request_21")
			.get(uri1 + "/maps-api-v3/api/js/27/8/stats.js")
			.headers(headers_1),
            http("request_22")
			.get(uri1 + "/maps/api/js/AuthenticationService.Authenticate?1shttp%3A%2F%2Fwww.xpanxion.com%2Fcontact%2F&callback=_xdc_._do80t1&token=8616")
			.headers(headers_22)))

	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}