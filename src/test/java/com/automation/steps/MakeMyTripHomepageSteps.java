package com.automation.steps;

import java.util.Calendar;
import java.util.Date;

import com.automation.pages.MakeMyTripHomepage;

import cucumber.api.java.en.Given;

public class MakeMyTripHomepageSteps {

	@Given("^user select '(.*)' tab on homepage$")
	public void user_select_tab_on_homepage(String tab) throws Throwable {

		MakeMyTripHomepage makeMyTripHomepage = new MakeMyTripHomepage();
		makeMyTripHomepage.selectTab(tab);
	}

}
