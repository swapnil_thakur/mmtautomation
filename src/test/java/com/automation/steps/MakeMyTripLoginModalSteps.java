package com.automation.steps;



import com.automation.pages.MakeMyTripHomepage;

import cucumber.api.java.en.Given;

public class MakeMyTripLoginModalSteps {

	@Given("^user landed into homepage$")
	public void user_landed_on_homepage() throws Throwable {

		MakeMyTripHomepage makeMyTripHomepage = new MakeMyTripHomepage();
		makeMyTripHomepage.verfiyHomePage();
	}

	@Given("^user get logged in using \\[loginInformation\\]$")
	public void user_get_logged_in_using_loginInformation() throws Throwable {
		MakeMyTripHomepage makeMyTripHomepage = new MakeMyTripHomepage();
		makeMyTripHomepage.selectLoginModal();
		makeMyTripHomepage.getMMTLoginModal().doLogin();
		
	}

}
