package com.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.automation.base.Base;
import com.automation.base.MMTWebElement;
import com.automation.utils.MMTUtils;

public class MakeMyTripLoginPage extends Base {

	WebDriver driver = getDriver();

	public MakeMyTripLoginPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@name='j_username']")
	public WebElement textboxUsername;

	@FindBy(xpath = "//input[@name='j_username']")
	public MMTWebElement textboxUsername1;
	
	
	@FindBy(xpath = "//input[@name='j_password']")
	public WebElement textboxPassword;

	@FindBy(xpath = "//select[@name='domain']")
	public WebElement dropDownDomain;

	@FindBy(xpath = "//input[@name='loginButton']")
	public WebElement buttonLogin;

	public WebElement getTextboxUsername() {
		return textboxUsername1;
	}

	public WebElement getTextboxPassword() {
		return textboxPassword;
	}

	public WebElement getDropDownDomain() {
		return dropDownDomain;
	}

	public WebElement getButtonLogin() {
		return buttonLogin;
	}

	public void doLogin() {
		getTextboxUsername().sendKeys("swapnilt");
		getTextboxPassword().sendKeys("spiderman123#");
		MMTUtils.selectDropdownByVisibleText(getDropDownDomain(), "XPANXIONMAIL");
		getButtonLogin().click();
		MMTUtils.sleep(2000);

	}

}
