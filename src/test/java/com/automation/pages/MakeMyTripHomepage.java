package com.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.automation.base.Base;
import com.automation.modals.MMTLoginModal;

public class MakeMyTripHomepage extends Base {

	WebDriver driver = getDriver();

	public MakeMyTripHomepage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[@class='logo']")
	public WebElement imgMMTLogo;

	public WebElement getImgMMTLogo() {
		return imgMMTLogo;
	}

	public WebElement getDropdownLogin() {
		return dropdownLogin;
	}

	@FindBy(xpath = "//*[@id='ssologinlink']")
	public WebElement dropdownLogin;

	public WebElement getTab(String tab) {
		String tabXpath = String.format("//*[contains(@class,'tab_txt') and contains(text(),'%s')]", tab);
		return driver.findElement(By.xpath(tabXpath));
	}

	public void verfiyHomePage() {
		getImgMMTLogo().isDisplayed();
	}

	public void selectTab(String tab) {
		getTab(tab).click();

	}
	
	public MMTLoginModal getMMTLoginModal(){
		return new MMTLoginModal();
	}

	public void selectLoginModal() {
		getDropdownLogin().click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}
