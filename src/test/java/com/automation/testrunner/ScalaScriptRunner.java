package com.automation.testrunner;
import java.io.IOException;
import java.util.Arrays;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

public class ScalaScriptRunner  {

	
	public static void main(String[] args) throws IOException{
		
		WebDriver driver;
  
	  /*  FirefoxProfile profile = new FirefoxProfile();
	   profile.setPreference("network.proxy.type",1);

	    profile.setPreference("network.proxy.http","localhost");

	    profile.setPreference("network.proxy.http_port",8181);*/

	   /* driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://www.google.co.in");
	
		//driver.get("https://www.google.co.in");
		
		//Runtime.getRuntime().exec("cmd /c start TestBat.bat");
*/		
		System.setProperty("webdriver.chrome.driver", ".\\server\\chromedriver.exe");
		
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--proxy-server=http://127.0.0.1:8181");
		
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		driver=new ChromeDriver(capabilities);
		driver.get("https://www.google.com");
	}
}
