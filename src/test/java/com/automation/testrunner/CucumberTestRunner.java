package com.automation.testrunner;

import java.io.IOException;

import org.junit.runner.RunWith;
import org.testng.IHookCallBack;
import org.testng.ITestContext;
import org.testng.ITestResult;

import cucumber.api.CucumberOptions;
import cucumber.api.Scenario;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@RunWith(Cucumber.class)
@CucumberOptions(features="cucumberscripts", glue="com/automation/steps", monochrome = true, plugin = { "pretty",
		"html:target/Destination" })
public class CucumberTestRunner extends AbstractTestNGCucumberTests {

	public static Scenario scenario;

	@Override
	public void run(IHookCallBack iHookCallBack, ITestResult iTestResult) {
		// TODO Auto-generated method stub
		super.run(iHookCallBack, iTestResult);
		System.out.println("**********************");
	}

	public void run_cukes(String features) throws IOException {
		// TODO Auto-generated method stub
		System.out.println(features);
		super.run_cukes();
		System.out.println("**********************ppppppp");

	}
}
