package com.automation.modals;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automation.base.Base;
import com.automation.utils.MMTUtils;

public class MMTLoginModal extends Base{


	public MMTLoginModal() {
		PageFactory.initElements(getDriver(), this);
	}

	@FindBy(xpath = "//input[@id='username']")
	public WebElement textboxUsername;

	@FindBy(xpath = "//*[@id='login_form_0']//input[@id='password_text']")
	public WebElement textboxPassword;

	@FindBy(xpath = "//input[@name='loginButton']")
	public WebElement buttonLogin;

	@FindBy(xpath = "//*[@id='login_form_0']//span[@class='chf_checkbox']")
	public WebElement checkBoxRememberMe;
	
	@FindBy(xpath = "//*[@id='login_form_0']//input[@id='login_btn' and @value='Continue']")
	public WebElement buttonContinue;
	
	
	public WebElement getTextboxUsername() {
		return textboxUsername;
	}

	public WebElement getCheckBoxRememberMe() {
		return checkBoxRememberMe;
	}

	public WebElement getButtonContinue() {
		return buttonContinue;
	}

	public WebElement getTextboxPassword() {
		return textboxPassword;
	}

	public WebElement getButtonLogin() {
		return buttonLogin;
	}

	public void doLogin() {
		getTextboxUsername().sendKeys("swapnilthakur62@gmail.com");
		MMTUtils.sleep(5000);
	
		getTextboxPassword().sendKeys("swapnil3128");
		getCheckBoxRememberMe().click();
		getButtonContinue().click();
		MMTUtils.sleep(2000);

	}
}
