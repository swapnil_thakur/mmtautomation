package com.automation.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class TestDataProvider {
	static Properties prop = new Properties();

	public static String GetString(String propName) {
		try {
			// load a properties file from class path, inside static method
			FileInputStream stream = new FileInputStream(new File("config/config.properties"));
			prop.load(stream);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return prop.getProperty(propName);
	}

}
