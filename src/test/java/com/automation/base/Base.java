package com.automation.base;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.input.XmlStreamReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.xml.sax.helpers.XMLReaderAdapter;

public class Base {

	public static WebDriver webDriver;

	public static XMLConfiguration dataConfig;
	public static PropertiesConfiguration propData;

	public synchronized static WebDriver getDriver() {

		if (webDriver == null) {
			webDriver = initWebdriver();
			//webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		return webDriver;
	}

	public static void launchApplication() {
		initWebdriver().get("https://www.makemytrip.com/");
	}

	private static WebDriver initWebdriver() {

		if (TestDataProvider.GetString("browser").equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\server\\chromedriver.exe");
			if (TestDataProvider.GetString("platform").equalsIgnoreCase("desktop")) {
				webDriver = new ChromeDriver();
				webDriver.get("https://www.makemytrip.com/");
				webDriver.manage().window().maximize();
			}
			if (TestDataProvider.GetString("platform").equalsIgnoreCase("mobile")) {
				Map<String, String> mobileEmulation = new HashMap<String, String>();
				mobileEmulation.put("deviceName", "Google Nexus 5");

				Map<String, Object> chromeOptions = new HashMap<String, Object>();
				chromeOptions.put("mobileEmulation", mobileEmulation);
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
				webDriver = new ChromeDriver(capabilities);
				webDriver.get("https://www.makemytrip.com/");
			}
		}

		else if (TestDataProvider.GetString("browser").equalsIgnoreCase("IE")) {
			System.setProperty("webdriver.ie.driver", ".\\server\\IEDriverServer.exe");
			webDriver = new InternetExplorerDriver();
		} else if (TestDataProvider.GetString("browser").equalsIgnoreCase("firefox")) {
			webDriver = new FirefoxDriver();
		}
		return webDriver;

	}

	public static void main(String args[]) throws IOException, URISyntaxException {
		loadResources();

		System.out.println(dataConfig.getProperty("features"));
		System.out.println(propData.getProperty("browser"));
		
		//String xyz=Base.class.getClassLoader().getResourceAsStream("/PerformanceAutomation/test1.har");
		System.out.println(getAbsoluteFilePath("/PerformanceAutomation/test1.har"));
		
	}

	public static String getAbsoluteFilePath(String classpathFile) throws URISyntaxException {
	    String absoluteFilePath = "";
	    URL url = Base.class.getClassLoader().getResource(classpathFile);
	    if (url != null) {
	        Path path = Paths.get(url.toURI());
	        if (path != null) {
	            absoluteFilePath = path.toString();
	        }
	    }
	    return absoluteFilePath;
	}
	
	public static void loadResources() {

		propData = new PropertiesConfiguration();
		dataConfig = new XMLConfiguration();
		try {
			propData.load(getFile("config/config.properties"));
			dataConfig.load(getFile("config/testng.xml"));
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static File getFile(String fileName) {
		return FileUtils.getFile(fileName);

	}

}