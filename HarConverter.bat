
set prop_reader_file=src\test\resources\config.properties
set output_simulation_folder=src\test\scala\simulations
set test_output_simulation_folder=Simulated_Scala_File
set resource_folder=src\test\resources

@ECHO OFF
FOR /F "tokens=1,2 delims==" %%A IN (%prop_reader_file%) DO (
    IF "%%A"=="har_file" SET har_file=%%B
    IF "%%A"=="output_simulation_file" SET output_simulation_file=%%B
    IF "%%A"=="project_directory" SET project_directory=%%B
)

echo %har_file%
set project_home=%project_directory%


recorder -of %project_home%\%test_output_simulation_folder%\ -m Har -hf %project_home%\%har_file% -cli true & echo abc

pause
endlocal
exit /b 0